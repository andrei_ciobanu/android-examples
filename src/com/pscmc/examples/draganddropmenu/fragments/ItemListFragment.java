package com.pscmc.examples.draganddropmenu.fragments;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.pscmc.examples.R;
import com.pscmc.examples.draganddropmenu.activity.ItemDetailActivity;
import com.pscmc.examples.draganddropmenu.component.DraggableListView;
import com.pscmc.examples.draganddropmenu.data.MySQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ItemListFragment extends Fragment implements AdapterView.OnItemClickListener, DraggableListView.DropListener {
    public static final String CUR_CHOICE = "curChoice";
    private DraggableListView listView;
    private boolean mDualPane;
    private int mCurCheckPosition = 0;

    private MySQLiteOpenHelper sqLiteOpenHelper;
    private MyBaseAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.item_list_fragment,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sqLiteOpenHelper = new MySQLiteOpenHelper(getActivity().getApplicationContext());

        listView = (DraggableListView) getView().findViewById(R.id.item_list_fragment_list);
        adapter = new MyBaseAdapter(getItems());
        listView.setAdapter(adapter);
        listView.setDropListener(this);

        View detailsFrame = getActivity().findViewById(R.id.details);
        mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        if (savedInstanceState != null) {
            mCurCheckPosition = savedInstanceState.getInt(CUR_CHOICE, 0);
        }

        if (mDualPane) {
            listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            showDetails(mCurCheckPosition, false);
        }

        listView.setOnItemClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sqLiteOpenHelper.close();
    }

    private List<Item> getItems() {
        SQLiteDatabase database = sqLiteOpenHelper.getReadableDatabase();
        Cursor query = database.query(MySQLiteOpenHelper.TABLE_NAME, MySQLiteOpenHelper.COLUMNS, null, null, null, null, MySQLiteOpenHelper.COLUMNS_PLACE);
        List<Item> returnList = new ArrayList<Item>(query.getCount());
        int i = 0;
        while (query.moveToNext()){
            Item item = new Item();
            item.setKey(query.getString(1));
            item.setValue(query.getString(2));
            item.setId(query.getInt(3));
            returnList.add(i, item);
            i++;
        }
        query.close();
        database.close();
        return returnList;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        showDetails(i, false);
    }

    private void showDetails(final int index, boolean forceUpdate) {
        mCurCheckPosition = index;

        if (mDualPane) {
            listView.clearFocus();
            listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.setItemChecked(index, true);
                }
            });

            ItemDetailFragment details = (ItemDetailFragment)
                    getFragmentManager().findFragmentById(R.id.details);
            if (details == null || details.getShownIndex() != index || forceUpdate) {
                details = ItemDetailFragment.newInstance(index);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.details, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }


        } else {
            Intent intent = new Intent();
            intent.setClass(getActivity(), ItemDetailActivity.class);
            intent.putExtra("index", index);
            startActivity(intent);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CUR_CHOICE, mCurCheckPosition);
    }

    @Override
    public void onDrop(int from, int to) {
        synchronized (adapter.getList()) {
            SQLiteDatabase database = sqLiteOpenHelper.getReadableDatabase();
            int i = 0;
            for(Item item : adapter.getList()){
                ContentValues values = new ContentValues();
                values.put(MySQLiteOpenHelper.COLUMNS_PLACE,i);
                database.update(MySQLiteOpenHelper.TABLE_NAME,values,MySQLiteOpenHelper.COLUMNS_ID + "="+item.getId(),null);
                i++;
            }

            database.close();
        }
        mCurCheckPosition = to;
        if(mDualPane){
            showDetails(mCurCheckPosition,true);
        }
    }
}
