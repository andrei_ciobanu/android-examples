package com.pscmc.examples.draganddropmenu.fragments;

public class Item {
    private String key;
    private String value;
    private int id;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Item){
            Item item = (Item) o;
            return key == null ? item.key == null : key.equals(item.key) && getId() == item.getId();
        }
        return false;
    }
}
