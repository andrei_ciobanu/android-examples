package com.pscmc.examples.draganddropmenu.fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.pscmc.examples.R;
import com.pscmc.examples.draganddropmenu.component.DraggableListView;

import java.util.List;

public class MyBaseAdapter extends DraggableListView.AbstractBaseAdapter<Item> {

    public MyBaseAdapter(List<Item> list) {
        super(list);
    }

    @Override
    protected Item createInvisible() {
        return new Item();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        synchronized (list) {
            if(view != null){
                setTextValuesForItem(i, view);
                return view;
            } else {
                LayoutInflater inflater = (LayoutInflater)viewGroup.getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                View newView = inflater.inflate(R.layout.list_item,null);
                setTextValuesForItem(i, newView);
                return newView;
            }
        }
    }

    private void setTextValuesForItem(int i, View view) {
        TextView textView = (TextView) view.findViewById(R.id.list_item_label);
        Item item = list.get(i);
        textView.setText(item.getKey());
    }

    public List<Item> getList() {
        return list;
    }


}
