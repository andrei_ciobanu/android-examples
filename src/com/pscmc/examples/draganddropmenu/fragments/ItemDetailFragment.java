package com.pscmc.examples.draganddropmenu.fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.pscmc.examples.R;
import com.pscmc.examples.draganddropmenu.data.MySQLiteOpenHelper;

public class ItemDetailFragment extends Fragment{
    private Context context;
    private TextView textView;

    private MySQLiteOpenHelper sqLiteOpenHelper;

    public static ItemDetailFragment newInstance(int index) {
        ItemDetailFragment f = new ItemDetailFragment();

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        context = getActivity().getApplicationContext();

        sqLiteOpenHelper = new MySQLiteOpenHelper(context);

        View thisView = inflater.inflate(R.layout.item_detail_fragment,container,false);
        textView = (TextView) thisView.findViewById(R.id.item_detail_fragment_text);
        final String text = getTextFromDB(getShownIndex());
        textView.setText(text);

        return thisView;
    }

    private String getTextFromDB(int index) {
        SQLiteDatabase database = sqLiteOpenHelper.getReadableDatabase();
        Cursor query = database.query(MySQLiteOpenHelper.TABLE_NAME, MySQLiteOpenHelper.COLUMNS, MySQLiteOpenHelper.COLUMNS_PLACE+"="+index, null, null, null, MySQLiteOpenHelper.COLUMNS_PLACE);
        String text = "";
        while (query.moveToNext()){
            text = query.getString(2);
        }
        query.close();
        database.close();
        return text;
    }

    public int getShownIndex() {
        return getArguments().getInt("index", 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        context = getActivity().getApplicationContext();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}

