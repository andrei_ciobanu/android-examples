package com.pscmc.examples.draganddropmenu.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.LinkedList;
import java.util.List;

/**
 * User: Andrei
 * Date: 9/19/12
 * Time: 8:58 PM
 */
public class DraggableListView extends ListView {

    private static final int END_OF_LIST_POSITION = -2;
    public static final int DELAY_MILLIS = 300;
    public static final int SPEED = 25;

    private int draggingItemHoverPosition;
    private int dragStartPosition;
    private Dragging dragging;

    Handler scrollHandler = new Handler();

    private int lastY;

    private int lastPosition;

    private Runnable scrollTimer = new Runnable() {
        @Override
        public void run() {
            scrollList(lastY);
            scrollHandler.postDelayed(this, DELAY_MILLIS);
        }
    };

    private View draggedItem;
    private DropListener dropListener;

    public DraggableListView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.listViewStyle);
    }

    public DraggableListView(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);



        setOnItemLongClickListener(new OnItemLongClickListener() {
            @SuppressWarnings("unused")
            @Override
            public boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
                int itemnum = pointToPosition(0, lastY);
                if (itemnum == AdapterView.INVALID_POSITION) {
                    return false;
                }

                if (dragging != null) {
                    dragging.stop();
                    dragging = null;
                }

                draggedItem = getChildAt(itemnum - getFirstVisiblePosition());
                draggedItem.setPressed(false);
                dragging = new Dragging(getContext());
                dragging.start(lastY, draggedItem);
                draggingItemHoverPosition = itemnum;
                dragStartPosition = draggingItemHoverPosition;

                AbstractBaseAdapter adapter = (AbstractBaseAdapter) getAdapter();
                adapter.remove(itemnum);
                adapter.notifyDataSetChanged();
                invalidate();
                return true;
            }
        });
    }


    public void drop(int from, int to) {
        AbstractBaseAdapter adapter = (AbstractBaseAdapter) getAdapter();
        adapter.moveTo(to);
        adapter.notifyDataSetChanged();
        if(dropListener != null){
            dropListener.onDrop(from,to);
        }
    }

    private int myPointToPosition(int x, int y) {
        if (y < 0) {
            return getFirstVisiblePosition();
        }
        Rect frame = new Rect();
        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            child.getHitRect(frame);
            if (frame.contains(x, y)) {
                return getFirstVisiblePosition() + i;
            }
        }
        if ((x >= frame.left) && (x < frame.right) && (y >= frame.bottom)) {
            return END_OF_LIST_POSITION;
        }
        return INVALID_POSITION;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        lastY = (int) ev.getY();

        if (dragging == null) {
            return super.onTouchEvent(ev);
        }


        switch (ev.getAction()) {

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                dragging.stop();
                dragging = null;

                if (draggingItemHoverPosition == END_OF_LIST_POSITION) {
                        drop(dragStartPosition, getCount() - 1);
                } else if (draggingItemHoverPosition != INVALID_POSITION) {
                        drop(dragStartPosition, draggingItemHoverPosition);
                }

                resetViews();
                break;

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                dragging.drag(lastY);
                int position = myPointToPosition(0, lastY);
                if (position != INVALID_POSITION) {
                    draggingItemHoverPosition = position;
                    if (lastPosition != position) {
                        lastPosition = position;
                        doExpansion(lastPosition);
                    }
                }

                scrollList(lastY);
                break;
        }

        return true;
    }

    private void doExpansion(int position) {
        AbstractBaseAdapter adapter = (AbstractBaseAdapter) getAdapter();
        if(position == END_OF_LIST_POSITION){
            adapter.setExpanded(getCount() - 1);
        } else {
            adapter.setExpanded(position);
        }
        invalidate();
        adapter.notifyDataSetChanged();
    }

    private void resetViews() {
        for (int i = 0; ; i++) {
            View v = getChildAt(i);
            if (v == null) {
                layoutChildren();
                v = getChildAt(i);
                if (v == null) {
                    break;
                }
            }
            ViewGroup.LayoutParams params = v.getLayoutParams();
            params.height = LayoutParams.WRAP_CONTENT;
            v.setLayoutParams(params);
        }
    }

    private void scrollList(int y) {

        int height = getHeight();

        int speed = 0;

        if (y < 0.2f * height) {
            speed = -getSpeed();
        } else if (y > 0.8f * height) {
            speed = getSpeed();
        }
        if (speed != 0) {
            int ref = pointToPosition(0, height / 2);
            if (ref == AdapterView.INVALID_POSITION) {
                ref = pointToPosition(0, height / 2 + getDividerHeight() + 64);
            }
            View v = getChildAt(ref - getFirstVisiblePosition());
            if (v != null) {
                int pos = v.getTop();
                setSelectionFromTop(ref, pos - speed);
            }

            scrollHandler.removeCallbacks(scrollTimer);
            scrollHandler.postDelayed(scrollTimer, DELAY_MILLIS);
        }


    }

    private int getSpeed() {
        int speed;
        speed = Math.max(getHeight(), getWidth()) / SPEED;
        return speed;
    }

    public void setDropListener(DropListener dropListener) {
        this.dropListener = dropListener;
    }

    private class Dragging {

        private Context context;
        private ImageView mDragView;
        private Bitmap mDragBitmap;
        private int mDragPoint;
        ViewGroup parent;
        AbsoluteLayout.LayoutParams params;

        public Dragging(Context context) {
            this.context = context;
            parent = (ViewGroup) getParent();
            params = new AbsoluteLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT,0,0);
        }

        public void start(int y, View view) {

            mDragView = new ImageView(context);
            mDragView.setBackgroundResource(android.R.drawable.alert_light_frame);

            view.setDrawingCacheEnabled(true);
            mDragBitmap = Bitmap.createBitmap(view.getDrawingCache());
            mDragView.setImageBitmap(mDragBitmap);

            mDragPoint = y - view.getTop();

            params.y = y - mDragPoint;

            parent.addView(mDragView, params);
        }

        public void drag(int y) {
            params.y = y - mDragPoint;
            parent.updateViewLayout(mDragView, params);
        }

        public void stop() {
            if (mDragView != null) {
                parent.removeView(mDragView);
                mDragView.setImageDrawable(null);
                mDragView = null;
            }
            if (mDragBitmap != null) {
                mDragBitmap.recycle();
                mDragBitmap = null;
            }
            scrollHandler.removeCallbacks(scrollTimer);
        }

    }

    public abstract static class AbstractBaseAdapter<I> extends BaseAdapter {

        protected final LinkedList<I> list = new LinkedList<I>();
        private I savedItem;
        private final I INVISIBLE = createInvisible();

        protected abstract I createInvisible();

        private boolean dirtyPreferences = false;

        public AbstractBaseAdapter(List<I> itemList) {
            list.addAll(itemList);
        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int i) {
            return list.get(i);
        }

        public long getItemId(int i) {
            return i;
        }

        public abstract View getView(int position, View convertView, ViewGroup parent);

        public void remove(int i) {
            synchronized (list){
                if(i>= 0 && i < list.size()){
                    savedItem = list.remove(i);
                    list.add(i,INVISIBLE);
                } else {
                    savedItem = list.remove(0);
                    list.add(0,INVISIBLE);
                }
            }
        }

        public void setExpanded(int position) {
            synchronized (list) {
                if (position >= 0 && position < list.size()) {
                    list.remove(INVISIBLE);
                    list.add(position, INVISIBLE);
                } else {
                    list.remove(INVISIBLE);
                    list.add(0, INVISIBLE);
                }
            }
        }

        public void moveTo(int to) {
            synchronized (list) {
                if (savedItem != null) {
                    if (to >= 0 && to < list.size()) {
                        list.add(to, savedItem);
                        list.remove(INVISIBLE);
                        dirtyPreferences = true;
                    } else {
                        list.add(0, savedItem);
                        list.remove(INVISIBLE);
                        dirtyPreferences = true;
                    }
                }
            }
        }
    }

    public abstract static interface DropListener {
        public void onDrop(int from, int to) ;
    }

}
