package com.pscmc.examples.draganddropmenu.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.pscmc.examples.R;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    private Context context;

    private static final int DATABASE_VERSION = 3;
    public static final String TABLE_NAME = "items";
    private static final String DATABASE_NAME = "data";

    public static String COLUMNS_PLACE = "place";
    public static String COLUMNS_KEY = "key";
    public static String COLUMNS_VALUE = "value";
    public static String COLUMNS_ID = "id";

    public static String[] COLUMNS = {COLUMNS_PLACE, COLUMNS_KEY, COLUMNS_VALUE,COLUMNS_ID};



    public MySQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLE_NAME+" (place INTEGER, key TEXT, value TEXT, id INTEGER);");

        String[] keyArray = context.getResources().getStringArray(R.array.keys);
        String[] valueArray = context.getResources().getStringArray(R.array.values);

        for (int i = 0; i< keyArray.length;i++){
            db.execSQL("INSERT INTO "+TABLE_NAME+" ("+COLUMNS_PLACE+","+ COLUMNS_KEY +","+ COLUMNS_VALUE +","+COLUMNS_ID+") VALUES (?,?,?,?);", new Object[]{i,keyArray[i],valueArray[i],i});
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE " + TABLE_NAME);
        onCreate(db);
    }
}
