package com.pscmc.examples.draganddropmenu.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ListView;
import com.pscmc.examples.R;

public class MainActivity extends FragmentActivity {
    private View mainLayout;
    private ListView listView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

}